import mysql.connector
import pandas as pd


mydb=mysql.connector.connect(
    host="localhost",
    user="root",
    passwd="Nightlight1alex",
    database="airbnb"
)

booking_list = {'booking_id': [29292929, 93483929], 'city': ["London", "Paris"], 'rate': [34.4,23.4], 'reviews': ["Great", "Horrible"], 'Superhost': [True, False],
                "Room_type": ["ladsjf", "kadljsf"], "guests": [3 , 4], "beds": [3.4, 3.4], "bedrooms": [3.9,4.3], "booking_url": ["falskdf", "dkaf"]}


mycursor = mydb.cursor()
mycursor.execute("show tables")
myresult = mycursor.fetchall()
existing_tables = [elem[0] for elem in myresult]

tables_to_make = ['Booking', 'Amenities', 'Host', 'Languages']
if tables_to_make[0] not in existing_tables:
    mycursor.execute("CREATE TABLE `Booking` ( \
                    `booking_id` int(8) NOT NULL, \
                    `city` varchar(30) NOT NULL, \
                    `rate` float, \
                    `reviews` int, \
                    `type_of_place_varchar` int, \
                    `superhost` boolean, \
                    `room_type` varchar(30), \
                    `guest` int, \
                    `beds` int, \
                    `bedrooms` float, \
                    `booking_url` varchar(300), \
                    PRIMARY KEY (`booking_id`, `city`) \
                    )")

if tables_to_make[1] not in existing_tables:
    mycursor.execute("CREATE TABLE `Amenities` ( \
                    `booking_id` int(8) NOT NULL, \
                    `city` varchar(30) NOT NULL, \
                    `wifi` boolean, \
                    `air_conditioning` boolean, \
                    `hot_water` boolean, \
                    `pool` boolean, \
                    `parking` boolean, \
                    `breakfast` boolean, \
                    `washer` boolean, \
                    `heating` boolean, \
                    `bathroom_essentials` boolean, \
                    `kitchen` boolean, \
                    `refrigerator` boolean, \
                    `fire_extinguisher` boolean, \
                    `carbon_monoxide_detector` boolean, \
                    `TV` boolean, \
                    `private_entrance` boolean, \
                    `laptop_friendly_workplace` boolean, \
                    `elevator` boolean, \
                    `building_staff` boolean, \
                    `host_greets_you` varchar(300), \
                    `garden_or_backyard` boolean, \
                    `patio_or_balcony` boolean, \
                     FOREIGN KEY (`booking_id`, `city`)  REFERENCES `Booking` (`booking_id`, `city`) )")


if tables_to_make[2] not in existing_tables:
    mycursor.execute("CREATE TABLE `Host` ( \
                       `host_id` int(8) NOT NULL, \
                       `booking_id` int(8) NOT NULL, \
                       `city` varchar(30) NOT NULL, \
                       `living` varchar(30), \
                       `languages` varchar(30), \
                       `verified` char, \
                       `host_url` varchar(300), \
                        PRIMARY KEY (`host_id`), \
                        FOREIGN KEY (`booking_id`, `city`)  REFERENCES `Booking` (`booking_id`, `city`) )")


if tables_to_make[3] not in existing_tables:
    mycursor.execute("CREATE TABLE `Languages` ( \
                    `host_id` int(8) NOT NULL, \
                    `speaks_english` boolean, \
                    `speaks_german` boolean, \
                    `speaks_hebrew` boolean, \
                    `speaks_french` boolean, \
                    `speaks_russia` boolean, \
                    `speaks_arabic` boolean, \
                     FOREIGN KEY (`host_id`)  REFERENCES `Host` (`host_id`) )")


