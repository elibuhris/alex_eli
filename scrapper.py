import conf
import requestor as rq
from bs4 import BeautifulSoup
import re
import sys


def pick_city(city):
    """This function get city and send request to airbnb place and check if it's available city and return the page f available else -1"""
    query_url = "https://www.airbnb.com/s/" + city + "/homes?refinement_paths%5B%5D=%2Fhomes&search_type=search_query"
    page = rq.url_request(query_url)
    soup = BeautifulSoup(page.text, 'html.parser')
    city_in_bar = soup.select("#Koan-via-SearchHeader__input")
    try:
        city_in_bar = city_in_bar[0]['value']
    except IndexError:
        print("Something went wrong")
        sys.exit(1)
    city_in_bar = city_in_bar[:-8]
    regex = re.compile('[^a-zA-Z]')
    city_in_bar = regex.sub('', city_in_bar)
    city = regex.sub('', city)
    city = city.lower()
    city_in_bar = city_in_bar.lower()
    if city in city_in_bar:
        print("The city was verified")
        return soup
    print("The city is not verified")
    return False


def get_amenities(source_code):
    amenity_list = ['Wifi', 'Air conditioning', 'Hot water', 'Pool', 'Parking', 'Breakfast', 'Washer', 'Heating',
                    'Bathroom essentials', 'Kitchen', 'Dishes and silverware', 'Refrigerator', 'Fire extinguisher',
                    'Carbon monoxide detector', 'TV', 'Private entrance', 'Laptop friendly workspace', 'Elevator',
                    'Building staff', 'Coffee maker', 'Host greets you', 'Garden or backyard', 'Patio or balcony']
    source_code = str(source_code)
    amenities = []
    for amenity in amenity_list:
        amenities_default = False
        if ('"name":"{}"'.format(amenity)) in source_code or ('"title":"{}"'.format(amenity)) in source_code:
            if ("Unavailable: {}".format(amenity)) not in source_code:
                amenities_default = "True"
        amenities.append(amenities_default)
    return amenities



def scraper(city_soup, user_preferences, city):
    """This function get soup of city and scrapes all the places in it and then returns following data:
    name, url, rate, #reviews, type of place, price, guests, beds, bedrooms, bathrooms,
    returns around MAX_RESULTS records by the user preferences if available
    """
    places = []
    hosts = []

    more_pages = True
    id_index = 0
    while more_pages:
        places_soup = city_soup.select("div._gig1e7")
        for place in places_soup:
            current_place = {}
            # current_host = {}
            print("Scraping Place id {}".format(id_index+1))
            current_place["booking_id"] = id_index
            current_place["city"] = city
            current_place["name"] = place.select("._1ebt2xej")[0].text
            try:
                current_place["rate"] = float(place.select("._ky9opu0")[0].text)
                current_place["reviews"] = int(place.select("._14e6cbz")[0].text[1:-1])
            except IndexError:
                current_place["rate"] = conf.MISSING_VALUE
                current_place["reviews"] = conf.MISSING_VALUE
            try:
                current_place["type_of_place"] = place.select("._1j3840rv")[0].text
            except IndexError:
                current_place["type_of_place"] = place.select("._1q6rrz5")[0].text
            current_place["price"] = place.select("._140n9qh")[0].text
            general = place.select("._1s7voim")[0].text
            general = general.split(" · ")
            if len(general) != 4:
                current_place["guests"] = conf.MISSING_VALUE
                current_place["beds"] = conf.MISSING_VALUE
                current_place["bedrooms"] = conf.MISSING_VALUE
                current_place["bathrooms"] = conf.MISSING_VALUE
            else:
                try:  # some people add strings (which are not always readable)
                    current_place["guests"] = int(general[0].split()[0])
                    current_place["bedrooms"] = general[1]
                    current_place["beds"] = int(float(general[2].split()[0]))  # i have no idea what is it 0.5 room but it's appears
                    current_place["bathrooms"] = float(general[3].split()[0])
                    current_place["bathrooms"] = float(general[3].split()[0])
                except ValueError:
                    current_place["guests"] = conf.MISSING_VALUE
                    current_place["beds"] = conf.MISSING_VALUE
                    current_place["bedrooms"] = conf.MISSING_VALUE
                    current_place["bathrooms"] = conf.MISSING_VALUE
            booking_url = place.select("a")[0].attrs["href"]
            current_place["booking_url"] = r"https://www.airbnb.com" + booking_url
            source_code = rq.get_page_source(current_place["booking_url"])
            current_place["amenities"] = get_amenities(source_code)

            # print("Scrapping host id {}".format(id_index+1))
            # current_host["host_id"] = id_index
            # place_url_page = rq.url_request(current_place["booking_url"])
            # place_url_soup = BeautifulSoup(place_url_page.text, 'html.parser')


            # host_url = [*map(lambda el: el.parent['href'], place_url_soup.select('._1ij6gln6 > a > img'))][0]


            # current_host["host_url"] = r"https://www.airbnb.com" + host_url
            # host_page = rq.url_request(current_host["host_url"])
            # host_soup = BeautifulSoup(host_page.text, 'html.parser')
            # try:
            #     current_host["living"] = host_soup.select("._910j1c5")[0].text
            # except IndexError:
            #     current_host["living"] = conf.MISSING_VALUE
            # try:
            #     languages = host_soup.select("._910j1c5")[1].text
            #     current_host["languages"] = create_language(languages)
            # except IndexError:
            #     current_host["languages"] = conf.MISSING_VALUE
            # try:
            #     current_host["verification"] = host_soup.select("._czm8crp")[1].text
            # except IndexError:
            #     current_host["verification"] = conf.MISSING_VALUE

            append_it = True
            for attribute in user_preferences.keys():
                if user_preferences[attribute]:
                    if user_preferences[attribute] != current_place[attribute]:
                        append_it = False
            if append_it:
                places.append(current_place)
                # hosts.append(current_host)
                id_index += 1

        print("{} places were scraped".format(len(places)))
        next_page = city_soup.select("li._r4n1gzb")[0]
        next_page = next_page.select("a")[0].attrs["href"]
        next_page = r"https://www.airbnb.com" + next_page
        try:
            page = rq.url_request(next_page)
            city_soup = BeautifulSoup(page.text, 'html.parser')
            if len(places) < conf.MAX_RESULTS:
                print("Moving to the next page...")
            else:
                more_pages = False
        except IndexError:
            more_pages = False
    # return places, hosts
    return places