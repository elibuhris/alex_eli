import conf
import mysql.connector

DB_NAME = "airbnb"
TABLE_PLACES = "places"
TABLE_AMENITIES = "amenities"

def delete_database():
    con = mysql.connector.connect(
        host="localhost",
        username=conf.USERNAME,
        password=conf.PASSWORD
    )

    db_cur = con.cursor()

    # Creates Database
    db_cur.execute("DROP DATABASE IF EXISTS {}".format(DB_NAME))
    con.commit()

    db_cur.execute("SHOW DATABASES")
    print("Data Bases:\n")
    for x in db_cur:
        print(x)
    db_cur.close()


def create_tables():
# Define parameters

    con = mysql.connector.connect(
        host="localhost",
        username=conf.USERNAME,
        password=conf.PASSWORD
    )

    db_cur = con.cursor()

    # Creates Database
    db_cur.execute("CREATE DATABASE IF NOT EXISTS {}".format(DB_NAME))
    con.commit()

    db_cur.execute("SHOW DATABASES")
    print("Data Bases:\n")
    for x in db_cur:
        print(x)

    db_cur.execute("""USE {}""".format(DB_NAME))
    con.commit()
    # Creates tables in the database

    db_cur.execute("""
                    CREATE TABLE IF NOT EXISTS {} (
                        'booking_id' int(8),
                        'city' varchar(30),
                        'name' varchar(30),
                        'rate' float,
                        'reviews' int,
                        'type_of_place' varchar(30),
                        'superhost' boolean,
                        'room_type' varchar(30),
                        'price' varchar(30),
                        'guest' int,
                        'beds' int,
                        'bedrooms' float,
                        'booking_url` varchar(300),
                        PRIMARY KEY ('booking_id', 'city')
                        )""".format(TABLE_PLACES))


    db_cur.execute("""
                    CREATE TABLE IF NOT EXISTS {} ( 
                    `booking_id` int(8) NOT NULL,
                    `city` varchar(30) NOT NULL
                    'Wifi' boolean,
                    'Air conditioning' boolean,
                    'Hot water' boolean, 
                    'Pool' boolean, 
                    'Parking' boolean, 
                    'Breakfast' boolean, 
                    'Washer' boolean, 
                    'Heating' boolean,
                    'Bathroom essentials' boolean, 
                    'Kitchen'boolean,
                    'Dishes and silverware' boolean, 
                    'Refrigerator' boolean, 
                    'Fire extinguisher' boolean,
                    'Carbon monoxide detector'boolean, 
                    'TV'boolean, 
                    'Private entrance' boolean, 
                    'Laptop friendly workspace' boolean,
                    'Elevator' boolean,
                    'Building staff' boolean,
                    'Coffee maker' boolean,
                    'Host greets you'varchar(300),
                    'Garden or backyard' boolean,
                    'Patio or balcony' boolean,
                    FOREIGN KEY (`booking_id`, `city`),
                    REFERENCES (`booking_id`, `city`)""".format(TABLE_AMENITIES))

    con.commit()
    db_cur.execute("SHOW TABLES")

    print("\nTables in {}:".format(DB_NAME))
    for table in db_cur:
        print(table)
    db_cur.close()


def place_data():

    con = mysql.connector.connect(
        host="localhost",
        username=conf.USERNAME,
        password=conf.PASSWORD
    )
    db_cur = con.cursor()

    db_cur.execute("""USE {}""".format(DB_NAME))
    con.commit()

    db_cur.execute("""
    INSERT INTO {}
    (`booking_id`,`city`,'Wifi','Air conditioning','Hot water','Pool','Parking',
    'Breakfast','Washer','Heating','Bathroom essentials','Kitchen','Dishes and silverware',
    'Refrigerator','Fire extinguisher','Carbon monoxide detector','TV','Private entrance',
    'Laptop friendly workspace','Elevator','Building staff','Coffee maker',
    'Host greets you','Garden or backyard','Patio or balcony') 
    VALUES ({},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{},{});
    """.format(TABLE_PLACES,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25))

    db_cur.execute("""
    INSERT INTO {}
    ( `booking_id`, `city`,'name',`rate`,`reviews` ,`type_of_place` ,`superhost` ,`room_type`,
    'price',`guest`,`beds`, `bedrooms`,`booking_url`) 
    VALUES ({},{},{},{},{},{},{},{},{},{},{},{},{});
    """.format(TABLE_AMENITIES,1,2,3,4,5,6,7,8,9,10,11,12,13))
    con.commit()

delete_database()
create_tables()


