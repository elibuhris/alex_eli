import mysql.connector

mydb = mysql.connector.connect(
  host = "localhost",
  user = "root",
  passwd = input(),
  database = "airbnb"
)

mycursor = mydb.cursor()
mycursor.execute("CREATE DATABASE practice")
for x in mycursor:
  print(x)

mycursor.execute("CREATE TABLE `Booking` ( \
                `booking_id` int(8) NOT NULL, \
                `city` varchar(30) NOT NULL, \
                `rate` float, \
                `reviews` int, \
                `type_of_place_varchar` int, \
                `superhost` boolean, \
                `room_type` varchar(30), \
                `guest` int, \
                `beds` int, \
                `bedrooms` float, \
                `booking_url` varchar(300), \
                PRIMARY KEY (`booking_id`, `city`) \
                )")

mycursor.execute("CREATE TABLE `Amenities` ( \
                `booking_id` int(8) NOT NULL, \
                `city` varchar(30) NOT NULL, \
                `wifi` boolean, \
                `air_conditioning` boolean, \
                `hot_water` boolean, \
                `pool` boolean, \
                `parking` boolean, \
                `breakfast` boolean, \
                `washer` boolean, \
                `heating` boolean, \
                `bathroom_essentials` boolean, \
                `kitchen` boolean, \
                `refrigerator` boolean, \
                `fire_extinguisher` boolean, \
                `carbon_monoxide_detector` boolean, \
                `TV` boolean, \
                `private_entrance` boolean, \
                `laptop_friendly_workplace` boolean, \
                `elevator` boolean, \
                `building_staff` boolean, \
                `host_greets_you` varchar(300), \
                `garden_or_backyard` boolean, \
                `patio_or_balcony` boolean, \
                 FOREIGN KEY (`booking_id`, `city`)  REFERENCES `Booking` (`booking_id`, `city`) )")


mycursor.execute("CREATE TABLE `Languages` ( \
                `host_id` int(8) NOT NULL, \
                `speaks_english` boolean, \
                `speaks_german` boolean, \
                `speaks_hebrew` boolean, \
                `speaks_french` boolean, \
                `speaks_russia` boolean, \
                `speaks_arabic` boolean, \
                 FOREIGN KEY (`host_id`)  REFERENCES `Host` (`host_id`) \
                )")

mycursor.execute("CREATE TABLE `Host` ( \
                `host_id` int(8) NOT NULL, \
                `booking_id` int(8) NOT NULL, \
                `city` varchar(30) NOT NULL, \
                `living` varchar(30), \
                `languages` varchar(30), \
                `verified` char, \
                `host_url` varchar(300), \
                 PRIMARY KEY (`host_id`), \
                 FOREIGN KEY (`booking_id`, `city`)  REFERENCES `Booking` (`booking_id`, `city`) )")
