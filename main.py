"""
Author:  Eli Buhris and Alexandra Brostoff
"""

import sys
import argparse
import conf
import scrapper as sc
import data_to_tables as dtc


def user_input():
    """This function returns analyzing the arg as user preferences."""
    parser = argparse.ArgumentParser()
    parser.add_argument('--city', type=str)
    parser.add_argument('--rating', type=float)
    parser.add_argument('--reviews', type=int)
    parser.add_argument('--type_of_place', choices=conf.TYPE_OF_PLACES)
    parser.add_argument('--guests', type=int)
    parser.add_argument('--bedrooms', choices=[int, 'Studio'])
    parser.add_argument('--beds', type=int)
    parser.add_argument('--bathrooms', type=int)
    args = parser.parse_args()
    if not args:
        print("usage: [--city] logfile")
        print("""optional logfile: 
        [--rating] [--reviews] [--type_of_place]  [--guests] [--bedrooms] [--beds] [--bathrooms]""")
        sys.exit(1)
        # [--min_price] [--max_price]
    if not args.city:
        print("usage: [--city] logfile")
        sys.exit(1)
    return vars(args)






def main():
    data_dict = False
    user_preferences = user_input()
    while not data_dict:
        city = user_preferences["city"]
        city_soup = sc.pick_city(user_preferences.pop("city"))
        if city_soup:
            places = sc.scraper(city_soup, user_preferences, city)
            data_dict = True
        else:
            print("The city you entered was not found in Airbnb")
            sys.exit(1)

        # booking_urls = [place["booking_url"] for place in places if "booking_url" in place.keys()]
    print(dtc.data_to_columns(places))


if __name__ == '__main__':
    main()

"""


"""