# DATA MINING: airbnb adventures


# The purpose of this project is to scrape data from:

https://www.airbnb.com.au/s/adventures
Here are the adventures offered by Airbnb:

# The scraped data will contain the following parameters: 
	* Name of the adventure
	* Location of the adventure (country and city)
	* Duration of the adventure 
	* Activity level of the adventure 
	* Host language of the adventure's host
	* What the adventure includes
	* The price of the adventure


# Files and requirements

The following requirements are needed:
Chrom -v 78
Python -v 3.6
Make sure you have following packages:
	* requests - v 2.22.0
	* BeautifulSoup4 - v 4.8.1
	* selenium - v 3.141.0
Furthermore the driver chromedriver.exe is needed and should be located in a directory under the name "drivers"  

If you do not have the ChromeDriver Installed, you can download it by following these instructions:
	1. On the Google Chrome browser, go to 'https://sites.google.com/a/chromium.org/chromedriver/'
	2. Download the correct driver for your chrome version. If you are unsure of your version, you can check by going to 'https://www.whatismybrowser.com/detect/what-version-of-chrome-do-i-have'


# Authors

Alexandra Brostoff - team member
Eli Buhris - team member

Enjoy your trip :)
