MAX_RESULTS = 1
MISSING_VALUE = None
URL = r"https://www.airbnb.com/s/homes"
import random
import requests
import re
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time
import json
import os
import sys
import argparse

description_amenities = {}
dictionary_amenities = {}
current_host = {}
language_dict = {}
host_language_dict = {}
dictionary_host = {}


def user_input():
    """This function returns analyzing the arg as user preferences."""
    str_to_bool = {'y': True, 'n': False}
    type_of_place = ["Entire place", "Private room", "Hotel room", "Shared room"]
    parser = argparse.ArgumentParser()
    parser.add_argument('--city', type=str)
    parser.add_argument('--rating', type=float)
    parser.add_argument('--reviews', type=int)
    parser.add_argument('--type_of_place', choices=type_of_place)
    parser.add_argument('--superhost', choices=str_to_bool.keys())
    parser.add_argument('--guests', type=int)
    parser.add_argument('--bedrooms', choices=[int, 'Studio'])
    parser.add_argument('--beds', type=int)
    parser.add_argument('--bathrooms', type=int)
    args = parser.parse_args()
    if not args:
        print("usage: [--city] logfile")
        print("""optional logfile: 
        [--rating] [--reviews] [--type_of_place] [--superhost] [--guests] [--bedrooms] [--beds] [--bathrooms]""")
        sys.exit(1)
        # [--min_price] [--max_price]
    if not args.city:
        print("usage: [--city] logfile")
        sys.exit(1)
    if args.superhost:
        args.superhost = str_to_bool[args.superhost]
    return vars(args)


def get_user_agent():
    """This function picks one user agent from a user agent's list"""
    # User agent chrome
    user_agent_list = [
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',
        'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36',
        'Mozilla/5.0 (Windows NT 5.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36',
        'Mozilla/5.0 (Windows NT 6.2; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.90 Safari/537.36',
        'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36',
        'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36',
        'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/57.0.2987.133 Safari/537.36',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',
        'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36'
    ]
    return {'user-agent': random.choice(user_agent_list)}


def url_request(url):
    """This function get URL and tries to send request to it"""
    while True:
        page = requests.get(url, headers=get_user_agent())
        if page:
            if not (page.status_code == 200):
                print("Something wrong  with the request")
                continue
        return page


def pick_city(city):
    """This function get city and send request to airbnb place and check if it's available city and return the page f available else -1"""
    query_url = "https://www.airbnb.com/s/" + city + "/homes?refinement_paths%5B%5D=%2Fhomes&search_type=search_query"
    page = url_request(query_url)
    soup = BeautifulSoup(page.text, 'html.parser')
    city_in_bar = soup.select("#Koan-via-SearchHeader__input")
    try:
        city_in_bar = city_in_bar[0]['value']
    except IndexError:
        print("Something went wrong")
        sys.exit(1)
    city_in_bar = city_in_bar[:-8]
    regex = re.compile('[^a-zA-Z]')
    city_in_bar = regex.sub('', city_in_bar)
    city = regex.sub('', city)
    city = city.lower()
    city_in_bar = city_in_bar.lower()
    if city in city_in_bar:
        print("The city was verified")
        return soup
    print("The city is not verified")
    return False


def make_soup(url_list):
    for url in url_list:
        page = url_request(url_list)
        soup = BeautifulSoup(page.text, 'html.parser')


def get_booking(city_soup, user_preferences):
    """This function get soup of city and scrapes all the places in it and then returns following data:
    name, url, rate, #reviews, type of place, price, guests, beds, bedrooms, bathrooms, superhost
    returns around MAX_RESULTS records by the user preferences if available
    """
    places = []
    more_pages = True
    id_index = 0
    while more_pages:
        places_soup = city_soup.select("div._1jl4bye9")
        for place in places_soup:
            current_place = {}
            current_place["booking_id"] = str(id_index + 1).zfill(8) #todo: later, have to figure out how to get the numbers to continue after we already scraped
            id_index += 1
            current_place["name"] = place.select("._1ebt2xej")[0].text
            try:
                current_place["rate"] = float(place.select("._ky9opu0")[0].text)
                current_place["reviews"] = int(place.select("._14e6cbz")[0].text[1:-1])
            except IndexError:
                current_place["rate"] = MISSING_VALUE
                current_place["reviews"] = MISSING_VALUE
            try:
                Superhost = place.select("._1b71k60")[0].text
                current_place["type_of_place"] = place.select("._1j3840rv")[0].text
                current_place["Superhost"] = True
            except IndexError:
                current_place["type_of_place"] = place.select("._1q6rrz5")[0].text
                current_place["superhost"] = False
            current_place["price"] = place.select("._140n9qh")[0].text
            general = place.select("._1s7voim")[0].text
            general = general.split(" · ")
            if len(general) != 4:
                current_place["guests"] = MISSING_VALUE
                current_place["beds"] = MISSING_VALUE
                current_place["bedrooms"] = MISSING_VALUE
                current_place["bathrooms"] = MISSING_VALUE
            else:
                try:  # some people add strings (which are not always readable)
                    current_place["guests"] = int(general[0].split()[0])
                    current_place["bedrooms"] = general[1]
                    current_place["beds"] = int(float(general[2].split()[0]))  # i have no idea what is it 0.5 room but it's appears
                    current_place["bathrooms"] = float(general[3].split()[0])  # i have no idea what is it 0.5 bath but it's appears
                    current_place["bathrooms"] = float(general[3].split()[0])  # i have no idea what is it 0.5 bath but it's appears
                except ValueError:
                    current_place["guests"] = MISSING_VALUE
                    current_place["beds"] = MISSING_VALUE
                    current_place["bedrooms"] = MISSING_VALUE
                    current_place["bathrooms"] = MISSING_VALUE
            booking_url = place.select("a")[0].attrs["href"]
            current_place["booking_url"] = r"https://www.airbnb.com" + booking_url
            append_it = True
            for attribute in user_preferences.keys():
                if user_preferences[attribute]:
                    if user_preferences[attribute] != current_place[attribute]:
                        append_it = False
            if append_it:
                places.append(current_place)
        print("{} places were scraped".format(len(places)))
        next_page = city_soup.select("li._r4n1gzb")[0]
        next_page = next_page.select("a")[0].attrs["href"]
        next_page = r"https://www.airbnb.com" + next_page
        try:
            page = url_request(next_page)
            city_soup = BeautifulSoup(page.text, 'html.parser')
            if len(places) < MAX_RESULTS:
                print("Moving to the next page...")
            else:
                more_pages = False
        except IndexError:
            more_pages = False
    booking_url_list = []
    booking_id_list = []
    #make places into a dataframe or json
    for item in places:
        booking_url_list.append(item['booking_url'])
        booking_id_list.append(item['booking_id'])
    return booking_url_list, booking_id_list


def make_soup(url_list):
    soup_list = []
    for url in url_list:
        page = requests.get(url, headers=get_user_agent())
        soup = BeautifulSoup(page.content, 'html.parser')
        soup_list.append(soup)
    return soup_list


def get_page_source(url_list):
    page_source_list = []
    for url in url_list:
        r = requests.get(url)
        page_source = r.text
        page_source = page_source.split('\n')
        page_source_list.append(page_source)
    return page_source_list


def make_id(page_source):
    id_index = 0
    id_list = []
    for num in page_source:
        id_list.append(str(id_index + 1).zfill(8))
        id_index +=1
    return id_list


def get_amenities(source_code_list, description_id_list):
    for description_id in description_id_list:
        for source_code in source_code_list:
            source_code = str(source_code)
            amenity_list = ['Wifi', 'Air conditioning', 'Hot water', 'Pool', 'Parking','Breakfast','Washer', 'Heating', 'Bathroom essentials', 'Kitchen', 'Dishes and silverware', \
                            'Microwave', 'Refrigerator', 'Shampoo', 'Lock on bedroom door', 'Fire extinguisher', 'Carbon monoxide detector', 'TV', \
                            'Private entrance', 'Laptop friendly workspace', 'Iron', 'Crib', 'High chair', 'Elevator', \
                            'Building staff', 'First aid kit', 'Coffee maker', 'Host greets you', 'Garden or backyard', 'Patio or balcony']
            for amenity in amenity_list:
                if ('"name":"{}"'.format(amenity)) in source_code or ('"title":"{}"'.format(amenity)) in source_code:
                    if ("Unavailable: {}".format(amenity)) not in source_code:
                        description_amenities[amenity]  = True
                    else:
                        description_amenities[amenity] = False
                else:
                    description_amenities[amenity] = False
            dictionary_amenities[description_id] = description_amenities
    print(dictionary_amenities)


def get_host_url(soup_code_list):
    host_url_list = []
    regex_host = r'([u][s][e][r][s][\/show]+\/[1-9{3,10}]+)'
    for url in soup_code_list:
        host_full_url = str(url.find_all(class_= '_16lonkd'))
        host_url = re.findall(regex_host, host_full_url)[0]
        host_url = 'https://www.airbnb.com/{}'.format(host_url)
        host_url_list.append(str(host_url))
    return host_url_list


def get_host_info(url_list, host_id_list, description_id_list):
    host_index=0
    for description_id in description_id_list:
        for url in url_list:
            page = url_request(url)
            soup = BeautifulSoup(page.text, 'html.parser')
            current_host = {}
            try:
                current_host["living"] = soup.select("._910j1c5")[0].text
            except IndexError:
                current_host["living"] = MISSING_VALUE
            try:
                current_host["languages"] = (soup.select("._910j1c5")[1].text)
                current_host["languages"] = current_host["languages"].split()[1:3]
            except IndexError:
                current_host["languages"] = MISSING_VALUE
                current_host["languages"] = MISSING_VALUE
            try:
                current_host["verification"] = soup.select("._czm8crp")[1].text
            except IndexError:
                current_host["verification"] = MISSING_VALUE
            dictionary_host[description_id] = current_host
    print(dictionary_host)
    print(current_host)
    return current_host["languages"]


def create_language(host_languages, host_id_list):
    language_list = ['English', 'Deutsch']
    for language in language_list:
        try:
            if language in host_languages:
                language_dict[language] = True
            else:
                language_dict[language] = False
        except NoneType:
            language_dict[language] = False
    host_index = 0
    for host_id in host_id_list:
        host_language_dict[host_id] = language_dict[host_index]
        host_index += 1
    print(host_language_dict)

def main():
    data_dict = False
    user_preferences = user_input()
    while not data_dict:
        city_soup = pick_city(user_preferences.pop("city"))
        if city_soup:
            scrape = get_booking(city_soup, user_preferences)
            booking_url_list = scrape[0]
            booking_id_list = scrape[1]
            soup_list = make_soup(booking_url_list)
            data_dict = True
            source_code_list = get_page_source(booking_url_list)
            description_id_list = booking_id_list
            get_amenities(source_code_list, description_id_list)
            host_url_list = get_host_url(soup_list)
            host_id_list = make_id(host_url_list)
            about_host = get_host_info(host_url_list, host_id_list, description_id_list)
            create_language(about_host, host_id_list)
        else:
            print("The city you entered was not found in Airbnb")
            sys.exit(1)