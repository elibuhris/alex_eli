import mysql.connector
import conf
import pandas as pd
class MyClass:
    def fill_tables(some_list):
        print(some_list)
        mydb = mysql.connector.connect(
            host="localhost",

            user=conf.USERNAME,
            passwd=conf.PASSWORD,

            user="root",
            passwd="",

            database="airbnb"
        )

        mycursor = mydb.cursor()
        sql_booking = "INSERT INTO booking (booking_id, city, name, rate, reviews,  type_of_place, superhost, room_type, guest,  \
              beds, bedrooms, booking_url) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
        sql_amenities = "INSERT INTO amenities (booking_id, city, wifi, air-conditioning, hot_water, pool, parking, breakfast,\
        washer, heating, bathroom_essentials, kitchen, refrigerator, fire_extinguisher, carbon_monoxide_detector, TV, private_entrance,\
        laptop_friendly_workplace, elevator, building_staff, host_greets_you, garden_or_backyard, patio_or_balcony) VALUES (%s, %s, %s, %s, %s, %s\
        %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"
        val = some_list
        mycursor.executemany(sql_booking, val)
        mycursor.execute((sql_amenities, val))
        mydb.commit()


mydb=mysql.connector.connect(
    host="localhost",
    user=conf.USERNAME,
    passwd=conf.PASSWORD,

    user="root",
    passwd="Nightlight1alex",

    database="airbnb"
)


mycursor = mydb.cursor()
mycursor.execute("show tables")
myresult = mycursor.fetchall()
existing_tables = [elem[0] for elem in myresult]

tables_to_make = ['Booking', 'Amenities']
if tables_to_make[0] not in existing_tables:

    mycursor.execute(

    mycursor.execute("CREATE TABLE `Booking` ( \
                    `booking_id` int(8) NOT NULL, \
                    `city` varchar(30) NOT NULL, \
                    `rate` float, \
                    `reviews` int, \
                    `type_of_place_varchar` int, \
                    `superhost` boolean, \
                    `room_type` varchar(30), \
                    `guest` int, \
                    `beds` int, \
                    `bedrooms` float, \
                    `booking_url` varchar(300), \
                    PRIMARY KEY (`booking_id`, `city`) \
                    )")


if tables_to_make[1] not in existing_tables:
    mycursor.execute("CREATE TABLE `Amenities` ( \
                    `booking_id` int(8) NOT NULL, \
                    `city` varchar(30) NOT NULL, \
                    `wifi` boolean, \
                    `air_conditioning` boolean, \
                    `hot_water` boolean, \
                    `pool` boolean, \
                    `parking` boolean, \
                    `breakfast` boolean, \
                    `washer` boolean, \
                    `heating` boolean, \
                    `bathroom_essentials` boolean, \
                    `kitchen` boolean, \
                    `refrigerator` boolean, \
                    `fire_extinguisher` boolean, \
                    `carbon_monoxide_detector` boolean, \
                    `TV` boolean, \
                    `private_entrance` boolean, \
                    `laptop_friendly_workplace` boolean, \
                    `elevator` boolean, \
                    `building_staff` boolean, \
                    `host_greets_you` varchar(300), \
                    `garden_or_backyard` boolean, \
                    `patio_or_balcony` boolean, \
                     FOREIGN KEY (`booking_id`, `city`)  REFERENCES `Booking` (`booking_id`, `city`) )")

